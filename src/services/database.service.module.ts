import { Module } from '@nestjs/common';
// import { InMemoryDatabaseServiceModule } from 'src/infrastructure/databases/in.memory/in.memory.database.module';
import { SqlDatabaseServiceModule } from 'src/infrastructure/databases/sql/sql.database.module';

@Module({
  // imports: [InMemoryDatabaseServiceModule],
  // exports: [InMemoryDatabaseServiceModule],
  imports: [SqlDatabaseServiceModule],
  exports: [SqlDatabaseServiceModule],
})
export class DatabaseServiceModule {}
