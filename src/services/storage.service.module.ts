import { Module } from '@nestjs/common';
import { AwsS3StorageServiceModule } from 'src/infrastructure/storage/aws/s3.module';

@Module({
  imports: [AwsS3StorageServiceModule],
  exports: [AwsS3StorageServiceModule],
})
export class StorageServiceModule {}
