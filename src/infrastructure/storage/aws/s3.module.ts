import { Module } from '@nestjs/common';
import { StorageService } from 'src/core/interfaces/storage';
import { AwsS3StorageService } from 'src/infrastructure/storage/aws/s3.service';
import { AwsS3ClientModule } from '../utils/aws.s3.client.module';

@Module({
  imports: [AwsS3ClientModule],
  providers: [{ provide: StorageService, useClass: AwsS3StorageService }],
  exports: [StorageService],
})
export class AwsS3StorageServiceModule {}
