import { Injectable } from '@nestjs/common';
import { PutObjectCommand } from '@aws-sdk/client-s3';
import { StorageService } from 'src/core/interfaces/storage';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import { AwsS3Client } from 'src/infrastructure/storage/utils/aws.s3.client.service';

@Injectable()
export class AwsS3StorageService implements StorageService {
  constructor(private s3Client: AwsS3Client) {}

  async generateSignedUrl(
    props: StorageService.GenerateSignedUrl,
  ): Promise<string> {
    const { key, bucket } = props;
    const command = new PutObjectCommand({ Bucket: bucket, Key: key });
    const signedUrl = getSignedUrl(this.s3Client, command, { expiresIn: 3600 });
    return signedUrl;
  }
}
