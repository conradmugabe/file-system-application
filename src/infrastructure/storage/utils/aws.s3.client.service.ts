import { S3Client } from '@aws-sdk/client-s3';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AwsS3Client extends S3Client {
  constructor() {
    super({ region: process.env.AWS_REGION });
  }
}
