import { Module } from '@nestjs/common';
import { AwsS3Client } from './aws.s3.client.service';

@Module({
  providers: [AwsS3Client],
  exports: [AwsS3Client],
})
export class AwsS3ClientModule {}
