import { Module } from '@nestjs/common';
import { DatabaseService } from 'src/core/interfaces/database';
import { SqlDatabaseService } from 'src/infrastructure/databases/sql/sql.database.service';
import { PrismaServiceModule } from 'src/infrastructure/databases/utils/prisma.service.module';

@Module({
  imports: [PrismaServiceModule],
  providers: [{ provide: DatabaseService, useClass: SqlDatabaseService }],
  exports: [DatabaseService],
})
export class SqlDatabaseServiceModule {}
