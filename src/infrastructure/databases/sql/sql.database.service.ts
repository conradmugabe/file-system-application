import { Injectable } from '@nestjs/common';
import { FileSystemNode } from 'src/core/entities';
import { DatabaseService } from 'src/core/interfaces/database';
import { PrismaService } from 'src/infrastructure/databases/utils/prisma.service';

@Injectable()
export class SqlDatabaseService implements DatabaseService {
  constructor(private readonly prismaService: PrismaService) {}

  createFolder(
    folder: DatabaseService.NewFileSystemNode,
  ): Promise<FileSystemNode> {
    return this.prismaService.fileSystemNode.create({ data: folder });
  }

  getById(
    props: DatabaseService.GetFileSystemNodeById,
  ): Promise<FileSystemNode> {
    return this.prismaService.fileSystemNode.findFirst({ where: props });
  }

  getFolderContents(
    props: DatabaseService.GetFolderContents,
  ): Promise<FileSystemNode[]> {
    return this.prismaService.fileSystemNode.findMany({ where: props });
  }

  renameFileSystemNode({
    id,
    name,
    path,
  }: DatabaseService.RenameFileSystemNode): Promise<FileSystemNode> {
    return this.prismaService.fileSystemNode.update({
      where: { id },
      data: { name, path },
    });
  }

  moveFolder({
    id,
    path,
    parentFolderId,
    pathIds,
  }: DatabaseService.MoveFolder): Promise<FileSystemNode> {
    return this.prismaService.fileSystemNode.update({
      where: { id },
      data: { path, parentFolderId, pathIds },
    });
  }

  addFile(file: DatabaseService.NewFile): Promise<FileSystemNode> {
    return this.prismaService.fileSystemNode.create({ data: file });
  }

  async deleteFileSystemNode({
    id,
  }: DatabaseService.GetFileSystemNodeById): Promise<void> {
    await this.prismaService.fileSystemNode.delete({ where: { id } });
  }
}
