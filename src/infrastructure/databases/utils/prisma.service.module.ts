import { Module } from '@nestjs/common';
import { PrismaService } from 'src/infrastructure/databases/utils/prisma.service';

@Module({
  providers: [PrismaService],
  exports: [PrismaService],
})
export class PrismaServiceModule {}
