import { Module } from '@nestjs/common';
import { DatabaseService } from 'src/core/interfaces/database';
import { InMemoryDatabaseService } from 'src/infrastructure/databases/in.memory/in.memory.database.service';

@Module({
  providers: [{ provide: DatabaseService, useClass: InMemoryDatabaseService }],
  exports: [DatabaseService],
})
export class InMemoryDatabaseServiceModule {}
