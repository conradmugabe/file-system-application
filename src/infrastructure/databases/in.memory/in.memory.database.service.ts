import { Injectable } from '@nestjs/common';
import { randomBytes } from 'crypto';
import { FileSystemNode } from 'src/core/entities';
import { DatabaseService } from 'src/core/interfaces/database';

@Injectable()
export class InMemoryDatabaseService implements DatabaseService {
  constructor(private data: FileSystemNode[] = []) {}

  createFolder(
    folder: DatabaseService.NewFileSystemNode,
  ): Promise<FileSystemNode> {
    const currentTime = new Date();
    const id = randomBytes(127).toString('hex');
    const fsNode: FileSystemNode = {
      id,
      createdAt: currentTime,
      updatedAt: currentTime,
      ...folder,
    };
    this.data.push(fsNode);
    return Promise.resolve(fsNode);
  }

  getById({
    id,
    ownedBy,
  }: DatabaseService.GetFileSystemNodeById): Promise<FileSystemNode> {
    const fsNode = this.data.find(
      (item) => item.id === id && item.ownedBy === ownedBy,
    );
    return Promise.resolve(fsNode);
  }

  getFolderContents({
    parentFolderId,
    ownedBy,
  }: DatabaseService.GetFolderContents): Promise<FileSystemNode[]> {
    const contents = this.data.filter(
      (content) =>
        content.parentFolderId === parentFolderId &&
        content.ownedBy === ownedBy,
    );
    return Promise.resolve(contents);
  }

  renameFileSystemNode({
    id,
    name,
    path,
  }: DatabaseService.RenameFileSystemNode): Promise<FileSystemNode> {
    const index = this.data.findIndex((content) => content.id === id);
    this.data[index].name = name;
    this.data[index].path = path;
    this.data[index].updatedAt = new Date();
    return Promise.resolve(this.data[index]);
  }

  moveFolder({
    id,
    path,
    parentFolderId,
    pathIds,
  }: DatabaseService.MoveFolder): Promise<FileSystemNode> {
    const index = this.data.findIndex((content) => content.id === id);
    this.data[index].path = path;
    this.data[index].parentFolderId = parentFolderId;
    this.data[index].pathIds = pathIds;
    this.data[index].updatedAt = new Date();
    return Promise.resolve(this.data[index]);
  }

  addFile(file: DatabaseService.NewFile): Promise<FileSystemNode> {
    const currentTime = new Date();
    const id = randomBytes(127).toString('hex');
    const fsNode: FileSystemNode = {
      id,
      createdAt: currentTime,
      updatedAt: currentTime,
      ...file,
    };
    this.data.push(fsNode);
    return Promise.resolve(fsNode);
  }

  deleteFileSystemNode({
    id,
    ownedBy,
  }: DatabaseService.GetFileSystemNodeById): Promise<void> {
    this.data = this.data.filter(
      (content) => content.id !== id && content.ownedBy !== ownedBy,
    );
    return Promise.resolve(undefined);
  }
}
