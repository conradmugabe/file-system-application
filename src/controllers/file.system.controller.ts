import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import {
  AddNewFileDto,
  CreateFolderDto,
  MoveFolderDto,
  RenameFileSystemNodeDto,
} from 'src/core/dto/file.system.dto';
import { UseCases } from 'src/use.cases/file.system/use.cases';

@Controller('api/v1/file-system')
export class FileSystemController {
  constructor(private useCases: UseCases) {}

  @Post('/folders')
  createFolder(@Body() data: CreateFolderDto) {
    const userId = '1';
    return this.useCases.createNewFolder({ userId, ...data });
  }

  @Post('/files')
  addNewFile(@Body() data: AddNewFileDto) {
    const userId = '1';
    return this.useCases.addNewFile({ userId, ...data });
  }

  @Get('/folders/contents/:id?')
  getFolderContents(@Param('id') id: string) {
    const userId = '1';
    return this.useCases.getFolderContents({ userId, id });
  }

  @Get('/:id')
  getFileSystemNodeDetails(@Param('id') id: string) {
    const userId = '1';
    return this.useCases.getFolderById({ userId, id });
  }

  @Patch('/:id')
  renameFileSystemNode(
    @Param('id') id: string,
    @Body() data: RenameFileSystemNodeDto,
  ) {
    const userId = '1';
    return this.useCases.renameFileSystemNode({ userId, id, ...data });
  }

  @Patch('/folders/:id')
  moveFolder(@Param('id') id: string, @Body() data: MoveFolderDto) {
    const userId = '1';
    return this.useCases.moveFolder({ userId, id, ...data });
  }

  @Delete('/:id')
  deleteFileSystemNode(@Param('id') id: string) {
    const userId = '1';
    return this.useCases.deleteFileSystemNode({ userId, id });
  }
}
