import { Controller, Get } from '@nestjs/common';
import { UseCases } from 'src/use.cases/uploads/use.cases';

@Controller('api/v1/uploads')
export class UploadController {
  constructor(private useCases: UseCases) {}

  @Get('/file')
  uploadFiles() {
    const userId = 'john.doe';
    const fileExtension = 'pdf';
    return this.useCases.uploadFiles({ userId, fileExtension });
  }
}
