import { Module } from '@nestjs/common';
import { FileSystemController } from './controllers/file.system.controller';
import { UploadController } from './controllers/upload.controller';
import { DatabaseServiceModule } from './services/database.service.module';
import { StorageServiceModule } from './services/storage.service.module';
import { UseCasesModule } from './use.cases/file.system/use.cases.module';
import { UseCasesModule as UploadUseCasesModule } from './use.cases/uploads/use.cases.module';

@Module({
  imports: [
    DatabaseServiceModule,
    StorageServiceModule,
    UseCasesModule,
    UploadUseCasesModule,
  ],
  controllers: [FileSystemController, UploadController],
  providers: [],
})
export class AppModule {}
