import { Module } from '@nestjs/common';
import { StorageServiceModule } from 'src/services/storage.service.module';
import { UseCases } from 'src/use.cases/uploads/use.cases';

@Module({
  imports: [StorageServiceModule],
  providers: [UseCases],
  exports: [UseCases],
})
export class UseCasesModule {}
