import { randomBytes } from 'crypto';
import { Injectable } from '@nestjs/common';
import { UploadFileDto } from 'src/core/dto/upload.dto';
import { StorageService } from 'src/core/interfaces/storage';

@Injectable()
export class UseCases {
  constructor(private storageService: StorageService) {}

  async uploadFiles({
    userId,
    fileExtension,
  }: UploadFileDto): Promise<UseCases.UploadFileResponse> {
    const bucket = process.env.AWS_BUCKET_NAME;
    const key = `${userId}/${randomBytes(32).toString('hex')}.${fileExtension}`;
    const signedUrl = await this.storageService.generateSignedUrl({
      bucket,
      key,
    });
    return { signedUrl };
  }
}

namespace UseCases {
  type SignedUrl = string;
  export type UploadFileResponse = { signedUrl: SignedUrl };
}
