import { Injectable } from '@nestjs/common';
import { FileSystemNode } from 'src/core/entities';
import { DatabaseService } from 'src/core/interfaces/database';

@Injectable()
export class UseCases {
  constructor(private databaseService: DatabaseService) {}

  async createNewFolder({
    name,
    userId,
    parentFolderId,
  }: UseCases.CreateFolderRequest): Promise<FileSystemNode> {
    let parentFolder: FileSystemNode;
    if (parentFolderId) {
      parentFolder = await this.getFolderById({
        userId,
        id: parentFolderId,
      });
    }
    const folderProps = this._createNewFolder(
      { name, userId, parentFolderId },
      parentFolder,
    );
    return this.databaseService.createFolder(folderProps);
  }

  async getFolderById({
    id,
    userId,
  }: UseCases.GetFolderContentsRequest): Promise<FileSystemNode> {
    const folder = await this._getFolderById({ id, ownedBy: userId });
    if (!folder) throw new Error('BAD REQUEST: Folder Not Found');

    return folder;
  }

  async getFolderContents({
    id,
    userId,
  }: UseCases.GetFolderContentsRequest): Promise<FileSystemNode[]> {
    if (id) await this.getFolderById({ id, userId });

    return this.databaseService.getFolderContents({
      ownedBy: userId,
      parentFolderId: id || userId,
    });
  }

  async renameFileSystemNode({
    id,
    userId,
    name,
  }: UseCases.RenameFileSystemNode): Promise<FileSystemNode> {
    const folder = await this.getFolderById({ id, userId });

    if (folder.name === name) return folder;

    const path = this._changePathTop(folder.path, name);
    return this.databaseService.renameFileSystemNode({ id, name, path });
  }

  async deleteFileSystemNode({
    id,
    userId,
  }: UseCases.GetFolderContentsRequest): Promise<void> {
    await this.getFolderById({ id, userId });
    return this.databaseService.deleteFileSystemNode({ id, ownedBy: userId });
  }

  async moveFolder({
    id,
    parentFolderId,
    userId,
  }: UseCases.MoveFolder): Promise<FileSystemNode> {
    const folder = await this.getFolderById({ id, userId });

    let parentFolder: FileSystemNode;
    if (parentFolderId) {
      parentFolder = await this.getFolderById({ id: parentFolderId, userId });

      if (folder.pathIds.includes(parentFolderId)) {
        throw new Error("Forbidden Error: Can't copy folder into self ");
      }
    }

    const props = this._moveFolder(userId, folder, parentFolder);
    return this.databaseService.moveFolder(props);
  }

  async addNewFile({
    userId,
    parentFolderId,
    fileUrl,
    name,
  }: UseCases.AddNewFileRequest) {
    let parentFolder: FileSystemNode;
    if (parentFolderId) {
      parentFolder = await this.getFolderById({
        userId,
        id: parentFolderId,
      });
    }
    const fileProps = this._addNewFile(
      { fileUrl, name, userId, parentFolderId },
      parentFolder,
    );
    return this.databaseService.addFile(fileProps);
  }

  private _getFolderById(
    props: DatabaseService.GetFileSystemNodeById,
  ): Promise<FileSystemNode | undefined> {
    return this.databaseService.getById(props);
  }

  private _generateFSNodePath(
    parentFolder: FileSystemNode,
    name: string,
    userId: string,
  ): string {
    if (parentFolder) return parentFolder.path + `/${name}`;

    return `/${userId}/${name}`;
  }

  private _generateFSNodePathIds(
    parentFolder: FileSystemNode,
    userId: string,
  ): string {
    if (parentFolder) return parentFolder.pathIds + `/${parentFolder.id}`;
    return `/${userId}`;
  }

  private _createNewFolder(
    { name, userId, parentFolderId }: UseCases.CreateFolderRequest,
    parentFolder: FileSystemNode,
  ): DatabaseService.NewFileSystemNode {
    const path = this._generateFSNodePath(parentFolder, name, userId);
    const pathIds = this._generateFSNodePathIds(parentFolder, userId);
    return {
      name,
      path,
      pathIds,
      type: 'folder',
      parentFolderId: parentFolderId || userId,
      createdBy: userId,
      ownedBy: userId,
      size: 0,
    };
  }

  private _addNewFile(
    { fileUrl, name, userId, parentFolderId }: UseCases.AddNewFileRequest,
    parentFolder: FileSystemNode,
  ): DatabaseService.NewFile {
    const newFile = this._createNewFolder(
      { name, userId, parentFolderId },
      parentFolder,
    );
    return { ...newFile, fileUrl };
  }

  private _moveFolder(
    userId: string,
    folder: FileSystemNode,
    parentFolder?: FileSystemNode,
  ) {
    const path = this._generateFSNodePath(parentFolder, folder.name, userId);
    const pathIds = this._generateFSNodePathIds(parentFolder, userId);
    return {
      id: folder.id,
      path,
      pathIds,
      parentFolderId: parentFolder?.id || userId,
    };
  }

  private _changePathTop(path: string, name: string) {
    const pathArr = path.split('/');
    pathArr.pop();
    pathArr.push(name);
    return pathArr.join('/');
  }
}

export namespace UseCases {
  export type CreateFolderRequest = {
    name: string;
    userId: string;
    parentFolderId?: string;
  };
  export type GetFolderContentsRequest = {
    id: string;
    userId: string;
  };
  export type RenameFileSystemNode = {
    id: string;
    name: string;
    userId: string;
  };
  export type MoveFolder = {
    id: string;
    parentFolderId: string;
    userId: string;
  };
  export type AddNewFileRequest = {
    userId: string;
    parentFolderId: string;
    fileUrl: string;
    name: string;
  };
}
