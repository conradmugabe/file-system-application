import { Module } from '@nestjs/common';
import { DatabaseServiceModule } from 'src/services/database.service.module';
import { UseCases } from 'src/use.cases/file.system/use.cases';

@Module({
  imports: [DatabaseServiceModule],
  providers: [UseCases],
  exports: [UseCases],
})
export class UseCasesModule {}
