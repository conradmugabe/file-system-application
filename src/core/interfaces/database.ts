import { FileSystemNode } from 'src/core/entities';

export abstract class DatabaseService {
  abstract getById(
    props: DatabaseService.GetFileSystemNodeById,
  ): Promise<FileSystemNode>;

  abstract createFolder(
    folder: DatabaseService.NewFileSystemNode,
  ): Promise<FileSystemNode>;

  abstract addFile(file: DatabaseService.NewFile): Promise<FileSystemNode>;

  abstract getFolderContents(
    props: DatabaseService.GetFolderContents,
  ): Promise<FileSystemNode[]>;

  abstract renameFileSystemNode(
    props: DatabaseService.RenameFileSystemNode,
  ): Promise<FileSystemNode>;

  abstract moveFolder(
    props: DatabaseService.MoveFolder,
  ): Promise<FileSystemNode>;

  abstract deleteFileSystemNode(
    props: DatabaseService.GetFileSystemNodeById,
  ): Promise<void>;
}

export namespace DatabaseService {
  export type NewFileSystemNode = {
    name: string;

    type: string;

    path: string;

    parentFolderId: string;

    createdBy: string;

    ownedBy: string;

    size: number;

    pathIds: string;
  };

  export type NewFile = NewFileSystemNode & { fileUrl: string };

  export type GetFileSystemNodeById = {
    id: string;

    ownedBy: string;
  };

  export type GetFolderContents = {
    parentFolderId: string;

    ownedBy: string;
  };

  export type RenameFileSystemNode = {
    id: string;

    name: string;

    path: string;
  };

  export type MoveFolder = {
    id: string;

    path: string;

    pathIds: string;

    parentFolderId: string;
  };
}
