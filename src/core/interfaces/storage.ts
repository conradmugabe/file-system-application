export abstract class StorageService {
  abstract generateSignedUrl(
    props: StorageService.GenerateSignedUrl,
  ): Promise<string>;
}

export namespace StorageService {
  export type GenerateSignedUrl = {
    bucket: string;

    key: string;
  };
}
