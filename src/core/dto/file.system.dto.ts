import { IsNotEmpty, IsString, IsOptional } from 'class-validator';

export class CreateFolderDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsOptional()
  parentFolderId: string;
}

export class GetFileSystemNodeByIdDto {
  @IsString()
  @IsNotEmpty()
  id: string;
}

export class GetFolderContentsDto extends GetFileSystemNodeByIdDto {}

export class RenameFileSystemNodeDto {
  @IsString()
  @IsNotEmpty()
  name: string;
}

export class MoveFolderDto {
  @IsString()
  @IsOptional()
  parentFolderId: string;
}

export class AddNewFileDto extends CreateFolderDto {
  @IsString()
  @IsNotEmpty()
  fileUrl: string;
}
