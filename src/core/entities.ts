export class FileSystemNode {
  id: string;

  createdAt: Date;

  updatedAt: Date;

  name: string;

  type: string;

  path: string;

  pathIds: string;

  parentFolderId: string;

  createdBy: string;

  ownedBy: string;

  size: number;

  fileUrl?: string;
}
