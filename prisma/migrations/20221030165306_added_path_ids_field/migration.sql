/*
  Warnings:

  - Added the required column `pathIds` to the `FileSystemNode` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_FileSystemNode" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "name" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "path" TEXT NOT NULL,
    "parentFolderId" TEXT NOT NULL,
    "createdBy" TEXT NOT NULL,
    "ownedBy" TEXT NOT NULL,
    "size" INTEGER NOT NULL,
    "pathIds" TEXT NOT NULL
);
INSERT INTO "new_FileSystemNode" ("createdAt", "createdBy", "id", "name", "ownedBy", "parentFolderId", "path", "size", "type", "updatedAt") SELECT "createdAt", "createdBy", "id", "name", "ownedBy", "parentFolderId", "path", "size", "type", "updatedAt" FROM "FileSystemNode";
DROP TABLE "FileSystemNode";
ALTER TABLE "new_FileSystemNode" RENAME TO "FileSystemNode";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
