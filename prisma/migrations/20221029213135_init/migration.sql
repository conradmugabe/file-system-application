-- CreateTable
CREATE TABLE "FileSystemNode" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "name" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "path" TEXT NOT NULL,
    "parentFolderId" TEXT NOT NULL,
    "createdBy" TEXT NOT NULL,
    "ownedBy" TEXT NOT NULL,
    "size" INTEGER NOT NULL
);
